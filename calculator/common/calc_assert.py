# @File ：calc_assert.py
# -*- ecoding: utf-8 -*-
# @Time: 2021/8/14 21:40
# @Author: niu run peng
import pytest
import allure

"""
断言类
"""


class CalculatorAssert:

    @staticmethod
    @allure.step('断言实际结果与预期结果相等')
    def assert_equal(result, expected):
        pytest.assume(result == expected)

    @staticmethod
    @allure.step('断言实际结果与预期结果不相等')
    def assert_not_equal(result, expected):
        pytest.assume(result != expected)

    @staticmethod
    @allure.step('断言实际结果与预期结果有交集')
    def assert_in(result, expected):
        pytest.assume(expected in result)
