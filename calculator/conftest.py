# @File ：conftest.py
# -*- ecoding: utf-8 -*-
# @Time: 2021/8/8 22:00
# @Author: niu run peng
import os
import sys
import logging
import pytest
import allure

sys.path.append(os.path.abspath(__file__).split('calculator')[0] + 'calculator/')
from calculator.api.calculator import Calculator
from common.yaml_operation import YamlOperation


@allure.step('实例化Calculator')
@pytest.fixture(scope='session')
def calc(cmdoption):
    """
    在测试开始前new一个Calculator对象 供测试用例调用
    :return: 返回Calculator实例对象
    """
    # 配置文件里配置Calculator类保留几位小数 是否四舍五入
    calc_config = cmdoption['calc']
    calc = Calculator(calc_config['keep_n'], calc_config['is_rounding'])
    return calc


@allure.step('打印计算状态')
@pytest.fixture(scope='function', autouse=True)
def print_calc():
    """
    方法开始前打印开始计算 方法结束后打印结束计算 也可在类中使用setup和teardown实现
    """
    logging.info('开始计算')
    yield
    logging.info('结束计算')


# 添加Calculator初始化命令行参数 是否四舍五入
def pytest_addoption(parser):
    my_group = parser.getgroup('test_options')
    my_group.addoption("--isRounding",
                       default='false',
                       dest='isRounding',
                       help='set your Calculator isRounding'
                       )


@pytest.fixture(scope='session')
def cmdoption(request):
    """
    isRounding为指定命令 可能值为false或者true 初始化Calculator使用 决定计算器保留小数时是否四舍五入
    :param request: 固定fixture 获取命令配置项
    :return: 返回Calculator配置项文件的字典数据
    """
    is_rounding = request.config.getoption("--isRounding", default='false')
    if is_rounding == 'false':
        con_path = '../config/con_not_rounding.yml'
    else:
        con_path = '../config/con_rounding.yml'
    config_data = YamlOperation.load_config(con_path)
    return config_data


def pytest_collection_modifyitems(items):
    """
    解决用例使用参数化装饰器时标题含有中文显示为unicode码的问题 也可以对用例进行其他高级操作
    :param items:
    :return:
    """
    for item in items:
        item.name = item.name.encode('utf-8').decode('unicode-escape')
        # 用例路径
        item._nodeid = item.nodeid.encode('utf-8').decode('unicode-escape')
