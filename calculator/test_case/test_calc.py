# @File ：test_calc.py
# -*- ecoding: utf-8 -*-
# @Time: 2021/8/14 15:25
# @Author: niu run peng
"""
计算器测试类
"""
import allure
import pytest
import logging

from common.get_attr import CalcGetattr as cg
from common.yaml_operation import YamlOperation as yamOpera
from common.calc_assert import CalculatorAssert as calcAss, CalculatorAssert


@pytest.mark.calculator
@allure.feature('计算器测试')
class TestCalculator:

    @allure.story('加法测试')
    @allure.title('{title}')
    @pytest.mark.parametrize(('title', 'add_list', 'expected'),
                             yamOpera.load_case('../case_data/calc_addition.yml',
                                                n=[1, 2, 3, 4, 5]), ids=yamOpera.case_title())
    # 备注:参数n代表指定某行或某些行测试用例被加载 便于报错的用例单独调试 不传就加载全部
    def test_addition(self, calc, title, add_list, expected):
        res = calc.addition(add_list)
        logging.info(f'{title}:{add_list} expected:{expected} real:{res}')
        calcAss.assert_equal(res, expected)

    @allure.story('减法测试')
    @allure.title('{title}')
    @pytest.mark.parametrize(('title', 'subtraction_list', 'expected'),
                             yamOpera.load_case('../case_data/calc_subtraction.yml'), ids=yamOpera.case_title())
    def test_subtraction(self, calc, title, subtraction_list, expected):
        res = calc.subtraction(subtraction_list)
        logging.info(f'{title}:{subtraction_list} expected:{expected} real:{res}')
        calcAss.assert_equal(res, expected)

    @allure.story('乘法测试')
    @allure.title('{title}')
    @pytest.mark.parametrize(('title', 'multiplication_list', 'expected'),
                             yamOpera.load_case('../case_data/calc_multiplication.yml'), ids=yamOpera.case_title())
    def test_multiplication(self, calc, title, multiplication_list, expected):
        res = calc.multiplication(multiplication_list)
        logging.info(f'{title}:{multiplication_list} expected:{expected} real:{res}')
        calcAss.assert_equal(res, expected)

    @allure.story('除法测试')
    @allure.title('{title}')
    @pytest.mark.parametrize(('title', 'division_list', 'assert_type', 'expected'),
                             yamOpera.load_case('../case_data/calc_division.yml'), ids=yamOpera.case_title())
    def test_division(self, calc, title, division_list, assert_type, expected):
        try:
            res = calc.division(division_list)
            # 可以在用例yml文件种配置决定使用CalculatorAssert类里哪种断言 反射调用该方法
            cg.calc_getattr(CalculatorAssert, assert_type, res, expected)
            logging.info(f'{title}:{division_list} expected:{expected} real:{res}')
        except Exception as e:
            # 当被测对象抛出异常时 断言该异常符合预期
            cg.calc_getattr(CalculatorAssert, assert_type, expected, str(e))
            logging.info(f'{title}:{division_list} expected:{expected} real:{str(e)}')
