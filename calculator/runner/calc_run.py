# @File ：calc_run.py
# -*- ecoding: utf-8 -*-
# @Time: 2021/8/15 9:07
# @Author: niu run peng
import os

import pytest

"""
文件方式运行pytest的运行入口
"""
if __name__ == '__main__':
    """
    isRounding为自定义cmd 可能值为false或者true 初始化Calculator使用 决定计算器保留小数时是否四舍五入
    """
    pytest.main(['-sqv', '--isRounding', 'true', '../test_case/test_calc.py', '-m', 'calculator',
                 '--alluredir', '../temp/'])
    os.system(f'allure generate  ../temp/  -o  ../report/  --clean')
