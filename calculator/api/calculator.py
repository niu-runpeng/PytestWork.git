# @File ：api.py
# -*- ecoding: utf-8 -*-
# @Time: 2021/8/14 11:33
# @Author: niu run peng
"""
计算器类 支持加、减、乘、除等计算
"""
import decimal
from typing import List
import allure


class Calculator:

    def __init__(self, keep_n: int = 2, is_rounding: bool = False):
        """
        初始化计算器
        :param keep_n: 结果保留n位小数 默认2位
        :param is_rounding: 是否四舍五入 True:四舍五入 False:不四舍五入 默认为False
        """
        self.keep_n = keep_n
        self.is_rounding = is_rounding

    @allure.step('调用加法')
    def addition(self, add_nums: List[float]) -> float:
        """
        计算器加法: 接受一个数据类型为float的列表 把列表中的元素相加求和
        :param
        add_nums: 要相加的数字列表
        :return: 返回相加的结果
        """
        if len(add_nums) == 1:
            return add_nums[0]
        res_add = 0
        for add_num in add_nums:
            res_add += add_num
        return self.rounding(res_add)

    @allure.step('调用减法')
    def subtraction(self, subtract_nums: List[float]) -> float:
        """
        计算器加法: 接受一个数据类型为float的列表 列表中的元素第一个一起减去其他元素
        :param
        subtract_nums: 要相减的数字列表
        :return:返回相减的结果
        """
        if len(subtract_nums) == 1:
            return subtract_nums[0]
        res_subtract = subtract_nums[0]
        for i in range(len(subtract_nums)):
            if i == 0:
                continue
            else:
                res_subtract = res_subtract - subtract_nums[i]
        return self.rounding(res_subtract)

    @allure.step('调用乘法')
    def multiplication(self, multiplication_nums: List[float]) -> float:
        """
        计算器乘法:接受一个数据类型为float的列表 列表中的元素依次相乘
        :param multiplication_nums: 要做乘法的参数列表
        :return: 返回相乘的结果
        """
        if len(multiplication_nums) == 1:
            return multiplication_nums[0]
        res_mul = multiplication_nums[0]
        for i in range(len(multiplication_nums)):
            if i == 0:
                continue
            else:
                res_mul = res_mul * multiplication_nums[i]
        return self.rounding(res_mul)

    @allure.step('调用除法')
    def division(self, division_nums: List[float]) -> float:
        """
        计算器除法:接受一个数据类型为float的列表 列表中的元素依次相乘
        :param division_nums:要做除法的参数列表
        :return:返回相除的结果
        """
        if len(division_nums) == 1:
            return division_nums[0]
        res_division = division_nums[0]
        for i in range(len(division_nums)):
            if i == 0:
                continue
            else:
                res_division = res_division / division_nums[i]
        return self.rounding(res_division)

    @allure.step('对结果保留小数位或进行四舍五入')
    def rounding(self, float_num: float) -> float:
        """
        操作小数截取位数、四舍五入
        :param float_num: 要保留位数或者四舍五入的参数
        :return: 返回经过保留位数或已四舍五入的结果
        """
        # 如果结果是小数才需要操作
        if '.' in str(float_num):
            # 获取入参小数部分
            sub_digit = str(float_num).split('.')[1]
            # 获取入参整数部分
            sub_int = str(float_num).split('.')[0]
            # 如果预期保留的位数大于小数部分长度再进行截取
            if self.keep_n < len(sub_digit):
                # 需要四舍五入
                if self.is_rounding:
                    decimal.getcontext().rounding = decimal.ROUND_HALF_UP
                    b = decimal.Decimal(float_num, decimal.getcontext())
                    return float(b.__round__(self.keep_n))
                # 不需要四舍五入 只截取
                else:
                    sub_digit = str(sub_digit[:self.keep_n])
                    return float(f'{sub_int}.{sub_digit}')
            # 不用截取 原值返回
            else:
                return float_num
        else:
            return float_num


if __name__ == '__main__':
    calc = Calculator(keep_n=4, is_rounding=True)
    print(calc.multiplication([calc.addition([1, 2]), calc.subtraction([18, 3])]))
    print(calc.addition([16, 2, 3]))
    print(calc.subtraction([16, 2, 3]))
    print(calc.multiplication([16.3, 2, 4.3]))
    print(calc.division([16, 2, 3]))
